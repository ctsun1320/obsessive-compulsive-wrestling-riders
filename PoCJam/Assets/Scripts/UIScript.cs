﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

    public PlayerMove player;
    public OpponentMove oppo;
    public Text collectText;
    public Text winText;
    public Text loseText;

    public AudioClip bgMusic;
    public AudioSource MusicSource;
	// Use this for initialization
	void Start () {
        if (player != null)
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMove>();
        if (oppo != null)
            oppo = GameObject.FindGameObjectWithTag("Enemy").GetComponent<OpponentMove>();

        MusicSource.clip = bgMusic;
        MusicSource.Play();
    }
	
	// Update is called once per frame
	void Update () {
        collectText.text = "Collected: " + player.collected;

        if (player.collected >= 10 && !loseText.enabled)
        {
            winText.enabled = true;
        }

        if (oppo.collected >= 6 && !winText.enabled)
        {
            loseText.enabled = true;
        }
	}
}
