﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OpponentMove : MonoBehaviour {

    public Transform goal;
    public NavMeshAgent agent;

    public Transform spawnPoint;

    public AudioClip[] hitSounds;
    public AudioSource sndSource;

    public int collected;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        agent.SetDestination(goal.position);
    }

    public void ChangeTarget()
    {
        //find the random collectible
        GameObject[] targetList = GameObject.FindGameObjectsWithTag("Collectible");
        //change target to that 
        int randomTarget = Random.Range(0, targetList.Length);
        GameObject newTarget = targetList[randomTarget];
        //set destination
        goal = newTarget.transform;
    }

    public void ChangeTarget(Transform another)
    {
        //find the random collectible
        GameObject[] targetList = GameObject.FindGameObjectsWithTag("Collectible");
        GameObject newTarget;
        //change target to that 
        int randomTarget = Random.Range(0, targetList.Length);
        newTarget = targetList[randomTarget];
        while (another == newTarget.transform)
        {
            randomTarget = Random.Range(0, targetList.Length);
            newTarget = targetList[randomTarget];
        }

        //set destination
        goal = newTarget.transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Punch")
        {
            int random = Random.Range(0, hitSounds.Length);
            sndSource.clip = hitSounds[random];
            sndSource.Play();
            this.transform.position = spawnPoint.position;
        }

        if (other.gameObject.tag == "Spikes")
        {
            this.transform.position = spawnPoint.position;
        }
    }

}
