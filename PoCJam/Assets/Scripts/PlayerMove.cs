﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float moveSpeed = 200.0f;
    public float turnSpeed = 60.0f;

    public int collected = 0;

    public GameObject punchLeft, punchRight;
    float punchX = 25.0f;
    bool hasPunched;

    public int punchTimer = 50;
    int curTimer = 0;

    public Transform playerSpawn;

    // Use this for initialization
    void Start () {
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        //movement and rotation
        float turnInput = Input.GetAxis("Horizontal");
        float moveInput = Input.GetAxis("Vertical");

        float moveRate = moveSpeed * moveInput * Time.deltaTime;
        float rotateRate = turnSpeed * turnInput * Time.deltaTime;

        transform.Rotate(transform.up, rotateRate);

        transform.Translate(-transform.forward * moveRate, Space.World);


        //changing punch dir

        Vector3 temp = Input.mousePosition;
        temp.z = 210.0f;
        //punchDir.transform.position = Camera.main.ScreenToWorldPoint(temp);
        Vector3 heading = Camera.main.ScreenToWorldPoint(temp) - this.transform.position;

        Vector3 leftPunchPos;
        Vector3 rightPunchPos;

        //Vector3 relativePoint = transform.InverseTransformPoint(Camera.main.ScreenToWorldPoint(temp));
        float relativePoint = LeftRightTest(this.transform.forward, heading, Vector3.up);
        if (Input.GetMouseButtonDown(0))
        {
            if (relativePoint < 0.0)
            {
                Debug.Log("Right");
            }
            else if (relativePoint > 0.0)
            {
                Debug.Log("Left");
            }
            else
                Debug.Log("Ahead/behind");

            if (!hasPunched)
            {
                punchLeft.SetActive(true);
                punchRight.SetActive(true);
                leftPunchPos = punchLeft.transform.localPosition;
                leftPunchPos.x += punchX;
                punchLeft.transform.localPosition = leftPunchPos;

                rightPunchPos = punchRight.transform.localPosition;
                rightPunchPos.x -= punchX;
                punchRight.transform.localPosition = rightPunchPos;
                hasPunched = true;
            }

        }
        //bug: if car is facing left, only left is registered. same for right

        //reverse this 
        if (hasPunched)
        {
            curTimer++;
            if (curTimer >= punchTimer)
            {
                leftPunchPos = punchLeft.transform.localPosition;
                leftPunchPos.x -= punchX;
                punchLeft.transform.localPosition = leftPunchPos;

                rightPunchPos = punchRight.transform.localPosition;
                rightPunchPos.x += punchX;
                punchRight.transform.localPosition = rightPunchPos;
                hasPunched = false;
                curTimer = 0;
                punchLeft.SetActive(false);
                punchRight.SetActive(false);
            }
        }

        //lock this's y and rotation x and z permanently
        Vector3 tempo = this.transform.position;
        tempo.y = 0;
        this.transform.position = tempo;

        tempo = this.transform.rotation.eulerAngles;
        tempo.x = 0;
        tempo.z = 0;
        this.transform.rotation = Quaternion.Euler(tempo);


    }

    public void CollectItem()
    {
        collected += 1;
    }

    float LeftRightTest(Vector3 fwd, Vector3 target, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, target);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            return 1.0f;
        }
        else if (dir < 0f)
        {
            return -1.0f;
        }
        else
        {
            return 0.0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Spikes")
        {
            this.transform.position = playerSpawn.position;
        }
    }
}
