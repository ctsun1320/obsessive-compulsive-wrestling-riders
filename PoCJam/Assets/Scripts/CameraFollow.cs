﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    Vector3 offset;
    //Vector3 rotateOffset;
	// Use this for initialization
	void Start () {
        offset.z = this.transform.position.z - target.transform.position.z;
        offset.y = this.transform.position.y - target.transform.position.y; //keep camera height constant
       // rotateOffset = this.transform.rotation.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = target.transform.position + offset;
        //this.transform.rotation = target.transform.rotation; //rotate with player
       // this.transform.Rotate(rotateOffset);
	}
}
