﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleScript : MonoBehaviour {

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerMove>().CollectItem();
            GameObject.FindGameObjectWithTag("Enemy").GetComponent<OpponentMove>().ChangeTarget(this.transform); //if this is what the opponent was searching, change its target to another
            Object.Destroy(this.gameObject);
        }

        if (other.tag == "Enemy")
        {
            other.GetComponent<OpponentMove>().ChangeTarget(this.transform);
            other.GetComponent<OpponentMove>().collected += 1;
            Object.Destroy(this.gameObject);
        }
    }
}
