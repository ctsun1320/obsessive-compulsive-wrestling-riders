﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleManager : MonoBehaviour {


    public GameObject[] potentialCollectibles;

    public int  spawnTimer = 500;

    int curTimer = 0;

    public int collectibleSpawn = 4;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < collectibleSpawn; i++)
        {
            int random = Random.Range(0, potentialCollectibles.Length);
            //create random collectible
            GameObject newCollectible = Instantiate(potentialCollectibles[random]) as GameObject;
            //create object at ransom spawn point
            GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnZone");

            newCollectible.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;

        }

        GameObject.FindGameObjectWithTag("Enemy").GetComponent<OpponentMove>().ChangeTarget();
    }
	
	// Update is called once per frame
	void Update () {
        if (curTimer >= spawnTimer)
        {
            for (int i = 0; i < collectibleSpawn; i++)
            {
                //when timer runs out
                int random = Random.Range(0, potentialCollectibles.Length);
                //create random collectible
                GameObject newCollectible = Instantiate(potentialCollectibles[random]) as GameObject;
                //create object at ransom spawn point
                GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnZone");

                newCollectible.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
            }

            curTimer = 0; //reset timer
        }
        //set timer
        else
        {
            curTimer++;
        }
    }
}
